package com.formation.ajc_jpa;

import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.formation.ajc_jpa.entities.Address;
import com.formation.ajc_jpa.entities.Event;
import com.formation.ajc_jpa.entities.Guest;
import com.formation.ajc_jpa.entities.User;
import com.formation.ajc_jpa.utils.HibernateUtils;

public class AppUser {
	public static Session s = null;

	public static void main(String[] args) {

		s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		try {
		
//			testCreateManyToOne();
			testManytoMany();
			
			tx.commit();
		} catch (Exception e) {
			if(tx != null) {
				try {
					tx.rollback();
				}catch (Exception e2) {
					// TODO: handle exception
					e2.printStackTrace();
				}
			}
			e.printStackTrace();
		} finally {
			s.close();
		}

	}

	private static void testManytoMany() {
		Event e1 = new Event("manytomany", "mkj", LocalDate.of(1978, Month.APRIL, 1), false, new Address(15, "qsdfsdf", 5644, "FRance"));

		Guest g1 = new Guest("JEsus", "qsdf@qsdf.qdf");
		Guest g2 = new Guest("JEsfdqsus", "qsdf@qsdf.qdf");

		List<Guest> gs = new ArrayList<Guest>();
		gs.add(g1);
		gs.add(g2);
		
		List<Event> es = new ArrayList<>();
		es.add(e1);

		e1.setGuests(gs);
		g1.setEvents(es);
		g2.setEvents(es);
		
		e1.getGuests().forEach(e -> System.out.println(e));

		s.persist(e1);
	}
	
	private static void create(Event e) {
		s.persist(e);
	}

	private static void update(int id, Event updatedEvent) {
		System.out.print("\n------ Update la table Event ------\n");
		Query<Event> q = s.createQuery("from Event where id=:myid");
		q.setParameter("myid", id);

		Event e = (Event) q.uniqueResult();

		e.setAllday(updatedEvent.isAllday()); // boolean type => never null
		if (updatedEvent.getTitle() != null)
			e.setTitle(updatedEvent.getTitle());
		if (updatedEvent.getDescription() != null)
			e.setDescription(updatedEvent.getDescription());
		if (updatedEvent.getBeginDate() != null)
			e.setBeginDate(updatedEvent.getBeginDate());

//		e = updatedEvent;
		s.merge(e);
//		System.out.println(s.getEntityName(object));
//		s.update(s.getEntityName(e), updatedEvent);

	}

	private static void delete(int id) {
		System.out.print("\n------ Delete la table Event ------\n");
		Query<Event> q = s.createQuery("from Event where id=:myid");
		q.setParameter("myid", id);

		Event e = (Event) q.uniqueResult();

		s.delete(e);
	}

	private static void read(int id) {
		System.out.print("\n------ ReadByID la table Event ------\n");
		Query<Event> q = s.createQuery("from Event where id=:myid");
		q.setParameter("myid", id);

		Event e = (Event) q.uniqueResult();

		System.out.println(e);

	}

	public static void printAll(List<Event> events) {
		System.out.print("\n------ PrintAll la table Event ------\n");
		for (Event event : events) {
			System.out.println(event.toString());
		}
	}

	private static void readAll() {
		System.out.print("\n------ ReadAll la table Event ------\n");
		Query<Event> q = s.createQuery("from Event");
		final List<Event> events = q.list();

		printAll(events);

	}

	private static void clean() {
		System.out.print("\n------ Vider la table Event ------\n");
		Query<Event> q = s.createQuery("delete Event");
		q.executeUpdate();
	}
	
	private static void testCreateManyToOne () throws InterruptedException  {
		Address a1 = new Address(15, "qsdfsdf", 5644, "FRance");
		Address a2 = new Address(150, "qsdfdsfsdf", 56544, "FRance");
		
		User u1 = new User("nicky", "123456", "nicky@kiki.qq", null);
		User u2 = new User("LuvLaetitia", "123456", "LAeti@kiki.qq", null);
		
		Event e1 = new Event("event", "qsdfqsdf", LocalDate.of(1978, Month.APRIL, 1), true, a1);
		Event e2 = new Event("event2", "qsdfqsdf", LocalDate.of(1978, Month.APRIL, 1), true, a2);
		Event e3 = new Event("event3", "qsdfqsdf", LocalDate.of(1978, Month.APRIL, 1), true, a1);
		
		e1.setUser(u1);
		e2.setUser(u1);
		e3.setUser(u2);
		
//		ArrayList<Event> eventListU1 = new ArrayList<Event>();
//		eventListU1.add(e1);
//		eventListU1.add(e2);
//		
//		ArrayList<Event> eventListU2 = new ArrayList<Event>();
//		eventListU1.add(e3);
//		
//		s.persist(u1);
//		s.persist(u2);
		s.persist(e1);
		s.persist(e2);
		s.persist(e3);
//		s.flush();
		s.refresh(u1);
		s.refresh(u2);
		
		//Thread.sleep(1000);
		printUsers();
	}

	private static void printUsers() {

		System.out.println(
				"**********************************************  [Users] ********************************************** ");

		// Create Query
		Query _query = s.createQuery("from User");
		ArrayList<User> _userList = (ArrayList<User>) _query.list();

		// Show data
		for (User User : _userList) {
			System.out.println(User.noListToString());

			if (User.getEventList() != null) {
				System.out.println("------------- Events ---------------");
				for (Event event : (List<Event>) User.getEventList()) {
					System.out.println("------------- Event Id : ---------------" + event.getId() + " title => "
							+ event.getTitle());
				}
			}
		}

	}
}
