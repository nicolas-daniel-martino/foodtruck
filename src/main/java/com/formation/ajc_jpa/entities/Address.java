package com.formation.ajc_jpa.entities;

import javax.persistence.*;

@Entity
public class Address {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "address_id")
	private int id;
	@Column(name = "address_streetNum")
	private int streetNum;
	@Column(name = "address_streetName")
	private String streetName;
	@Column(name = "address_postCode")
	private int postCode;
	@Column(name = "address_country")
	private String country;

	@ManyToOne
	@JoinColumn(name="clients_id")
	private Client client;

	public Address() {	}

	public Address(int streetNum, String streetName, int postCode, String country) {
		setStreetNum(streetNum);
		setStreetName(streetName);
		setPostCode(postCode);
		setCountry(country);
	}

	public int getStreetNum() {
		return streetNum;
	}

	public void setStreetNum(int streetNum) {

		this.streetNum = streetNum;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public int getPostCode() {
		return postCode;
	}

	public void setPostCode(int postCode)  {
		this.postCode = postCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "Address [id=" + id + ", streetNum=" + streetNum + ", streetName=" + streetName + ", postCode="
				+ postCode + ", country=" + country + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + id;
		result = prime * result + postCode;
		result = prime * result + ((streetName == null) ? 0 : streetName.hashCode());
		result = prime * result + streetNum;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Address))
			return false;
		Address other = (Address) obj;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (id != other.id)
			return false;
		if (postCode != other.postCode)
			return false;
		if (streetName == null) {
			if (other.streetName != null)
				return false;
		} else if (!streetName.equals(other.streetName))
			return false;
		if (streetNum != other.streetNum)
			return false;
		return true;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

}
