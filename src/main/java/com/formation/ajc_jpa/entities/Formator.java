package com.formation.ajc_jpa.entities;

import java.util.List;

import javax.persistence.*;

@Entity
public class Formator {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "formators_id")
	private int id;

	@Column(name = "formators_name")
	private String name;


	@ManyToMany
	@JoinTable(
			name="formateurs_formations",
			joinColumns=@JoinColumn(name="formators_id"), 
			inverseJoinColumns = @JoinColumn(name="formation_id"))
	private List<Formation> formations;

	public Formator() {
		super();
	}

	public Formator(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Formator [id=" + id + ", name=" + name + "]";
	}

	public List<Formation> getFormations() {
		return formations;
	}

	public void setFormations(List<Formation> formations) {
		this.formations = formations;
	}

}
