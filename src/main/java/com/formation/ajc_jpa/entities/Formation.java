package com.formation.ajc_jpa.entities;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.*;

@Entity
public class Formation {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "formation_id")
	private int id;

	@Column(name = "formation_nom")
	private String nom;

	@Column(name = "formation_startDate")
	private LocalDate startDate;

	@Column(name = "formation_endDate")
	private LocalDate endDate;

	@OneToMany(mappedBy = "formation", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
	private List<Etudiant> etudiants;

	@ManyToMany(mappedBy = "formations", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
	private List<Formator> formateurs;

	public Formation() {

	}

	public void addEtudiant() {

	}

	public Formation(String nom, LocalDate startDate, LocalDate endDate) {

		this.nom = nom;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public List<Etudiant> getEtudiants() {
		return etudiants;
	}

	public void setEtudiants(List<Etudiant> etudiants) {
		this.etudiants = etudiants;
	}

	@Override
	public String toString() {
		return "Formation [id=" + id + ", nom=" + nom + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", etudiants=" + etudiants + "]";
	}

	public List<Formator> getFormateurs() {
		return formateurs;
	}

	public void setFormateurs(List<Formator> formateurs) {
		this.formateurs = formateurs;
	}

}
