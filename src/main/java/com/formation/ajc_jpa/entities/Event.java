package com.formation.ajc_jpa.entities;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.*;

@Entity
public class Event {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="events_id")
	private int id;
	
	@Column(name="events_title", nullable = false)
	private String title;
	
	@Column(name="events_description")
	private String description;
	
	@Column(name="events_beginDate")
	private LocalDate beginDate;
	
	@Column(name="events_allDay")
	private boolean allday;

	//Relations/
	@ManyToMany(mappedBy = "events", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	private List<Guest> guests;
	
	@ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name="user", referencedColumnName = "users_id")
	private User user;
	
	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	@JoinColumn(name="address_id",
	referencedColumnName = "address_id")
	private Address address;
	
	public Event() {
		super();
	}

	public Event( String title, String description, LocalDate beginDate, boolean allday, Address address) {
		super();
		this.title = title;
		this.description = description;
		this.beginDate = beginDate;
		this.allday = allday;
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(LocalDate beginDate) {
		this.beginDate = beginDate;
	}

	public boolean isAllday() {
		return allday;
	}

	public void setAllday(boolean allday) {
		this.allday = allday;
	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", title=" + title + ", description=" + description + ", beginDate=" + beginDate
				+ ", allday=" + allday + "]";
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Guest> getGuests() {
		return guests;
	}

	public void setGuests(List<Guest> guests) {
		this.guests = guests;
	}
}
