package com.formation.ajc_jpa.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "users_id")
	private int id;
	@Column(name = "users_login")
	private String login;
	@Column(name = "users_pass")
	private String pass;
	@Column(name = "users_email")
	private String email;

	@OneToMany(mappedBy = "user")
	private List<Event> eventList;

	public User() {
		super();
	}



	public User( String login, String pass, String email, List<Event> eventList) {

		this.login = login;
		this.pass = pass;
		this.email = email;
		this.eventList = eventList;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", login=" + login + ", pass=" + pass + ", email=" + email + ", eventList="
				+ eventList + "]";
	}

	public String noListToString() {
		return "User [id=" + id + ", login=" + login + ", pass=" + pass + ", email=" + email + "]";
	}

	public List<Event> getEventList() {
		return eventList;
	}

	public void setEventList(List<Event> eventList) {
		this.eventList = eventList;
	}

}
