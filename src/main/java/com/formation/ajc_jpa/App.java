package com.formation.ajc_jpa;

import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.formation.ajc_jpa.entities.Address;
import com.formation.ajc_jpa.entities.Client;
import com.formation.ajc_jpa.entities.Etudiant;
import com.formation.ajc_jpa.entities.Event;
import com.formation.ajc_jpa.entities.Formation;
import com.formation.ajc_jpa.entities.Formator;
import com.formation.ajc_jpa.entities.User;
import com.formation.ajc_jpa.utils.HibernateUtils;

public class App {
	public static Session s = null;

	public static void main(String[] args) {

		s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		try {

			testOneToMany();

			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				try {
					tx.rollback();
				} catch (Exception e2) {
					// TODO: handle exception
					e2.printStackTrace();
				}
			}
			e.printStackTrace();
		} finally {
			s.close();
		}

	}
	private static void testOneToMany() {
		Address a1 = new Address(15, "qsdfsdf", 5644, "FRance");
		Address a2 = new Address(150, "qsdfdsfsdf", 56544, "FRance");
		
		List<Address> address = new ArrayList<Address>(Arrays.asList(a1, a2));
		
		Client c1 = new Client("Nicky", "Martini", "nik@qsdf.qsdf", "0149776269", address);
		a1.setClient(c1);
		a2.setClient(c1);
		s.persist(c1);
	}
}
